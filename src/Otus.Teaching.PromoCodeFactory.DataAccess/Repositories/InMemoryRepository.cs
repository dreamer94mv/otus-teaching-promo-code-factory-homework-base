﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T entity)
        {
            var result = true;

            if (Data.Any(x => x.Id == entity.Id))
                result = false;
            else
                Data = Data.Append(entity);

            return Task.FromResult(result);
        }

        public Task<bool> UpdateAsync(T entity)
        {
            var result = true;

            var entityFromDb = Data.FirstOrDefault(x => x.Id == entity.Id);

            if (entityFromDb == null)
                result = false;
            else
            {
                var temporyList = Data.ToList();
                var indexEntityFromDb = temporyList.IndexOf(entityFromDb);
                temporyList[indexEntityFromDb] = entity;
                Data = temporyList.AsEnumerable();
            }

            return Task.FromResult(result);
        }

        public Task DeleteByIdAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();
            
            return Task.CompletedTask;
        }
    }
}