﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeEditRequest:EmployeeCreateRequest
    {
        public Guid Id { get; set; }
    }
}
