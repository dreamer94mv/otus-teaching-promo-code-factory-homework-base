﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IMapper _mapper;

        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync([FromBody] EmployeeCreateRequest employeeRequestData)
        {
            try
            {
                var employeeModel = _mapper.Map<Employee>(employeeRequestData);

                var roles = await _rolesRepository.GetAllAsync();

                if (employeeModel.Roles.Any(employeeModelPredicate =>
                        roles.All(c => c.Id != employeeModelPredicate.Id)))
                {
                    return BadRequest("Одна или несколько ролей не существует. Не удалось создать запись о сотруднике.");
                }

                if (!await _employeeRepository.CreateAsync(employeeModel))
                    return Problem("Не удалось создать запись о сотруднике.");
                return Ok();
            }
            catch (Exception problemException)
            {
                return Problem(problemException.Message);
            }
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut()]
        public async Task<ActionResult> UpdateEmployeeByIdAsync(EmployeeEditRequest employeeRequestData)
        {

            try
            {
                var roles = await _rolesRepository.GetAllAsync();

                if (employeeRequestData.Roles.Any(employeeModelPredicate =>
                        roles.All(c => c.Id != employeeModelPredicate.Id)))
                {
                    return BadRequest("Одна или несколько ролей не существует. Не удалось обновить запись о сотруднике.");
                }

                var entity = await _employeeRepository.GetByIdAsync(employeeRequestData.Id);

                if (entity == null)
                    return NotFound();

                var editEmployee = _mapper.Map<Employee>(employeeRequestData);

                var result = await _employeeRepository.UpdateAsync(editEmployee);

                if (result)
                    return Ok();
                else
                    return Problem("Не удалось внести изменения");

            }
            catch (Exception problemException)
            {
                return Problem(problemException.Message);
            }
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var entity = await _employeeRepository.GetByIdAsync(id);

            if (entity == null)
                return NotFound();

            await _employeeRepository.DeleteByIdAsync(id);

            return Ok();
        }
    }
}