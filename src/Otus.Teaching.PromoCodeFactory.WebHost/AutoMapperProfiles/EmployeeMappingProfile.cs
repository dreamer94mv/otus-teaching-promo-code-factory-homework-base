﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.AutoMapperProfiles
{
    public class EmployeeMappingProfile : Profile
    {

        public EmployeeMappingProfile()
        {
            CreateMap<EmployeeCreateRequest, Employee>()
                .ForMember(
                    destinationMember => destinationMember.Id,
                    options => options.MapFrom((requestModel) => Guid.NewGuid()));
            CreateMap<EmployeeEditRequest, Employee>();
            CreateMap<EmployeeCreateRoleRequest, Role>();
        }
    }
}
